var nav = document.querySelector("#nav");
var navItems = nav.querySelectorAll("li");
var hamburgerButton = nav.querySelector("#hamburger-button");
console.log(navItems);
// We add an event listener for each nav-item
navItems.forEach(function(navItem) {
    navItem.addEventListener("click",activateItem);
});

function activateItem(event) {
    // close the burger menu
    hamburgerButton.checked = false;
}

